<?php
/*
    Yazhi NCC Demo
    Author : vkajamugan@yazhii.net
    Description : This PHP implement the NCC init request for Re Direct method.
*/

    include "RestClient.php";

    // Payment gateway API credentials

    $jsonRequest['clientId']="XXXX";
    $jsonRequest['token']="token_xxxxxxxx";
    $jsonRequest['secret']="secret_xxxxx";


    $jsonRequest['requestType']="NCC_QUERY";
    $jsonRequest['filterBy']='clientReference';
    $jsonRequest['query']='yazhi0000123';
    $jsonResponse = RestClient::sendRequest("https://www.ipayos.com/ncc_controller.php", json_encode($jsonRequest));
    echo $jsonResponse;
?>
